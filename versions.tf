terraform {
  required_providers {
    opentelekomcloud = {
      source = "terraform-providers/opentelekomcloud"
    }
  }
  required_version = ">= 0.13"
}
