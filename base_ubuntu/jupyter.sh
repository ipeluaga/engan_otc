# ------------------------------------ Run jupyter notebook # to kill it https://github.com/jupyter/notebook/issues/2844
installJupyter(){
  conda install -y -c conda-forge notebook
  #pip3 install setuptools ; pip3 install notebook
}

installJupyter

# ------------------------------------ Run jupyter notebook # to kill it https://github.com/jupyter/notebook/issues/2844
#env:
export PATH=/root/anaconda3/bin:$PATH # TODO: now running as root but it should be ubuntu or centos
export LD_LIBRARY_PATH=/usr/local/cuda-10.1/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export PYTHONPATH=/root/progressive_growing_of_gans

nohup jupyter-notebook --allow-root --no-browser > /root/jupyterLogs 2>&1 &

# Wait for the notebook to be ready...
sleep 10

# Get the notebook endpoint
jupyter-notebook list # TODO: take from here the localhost url and display it together with the logs below


allowTcpFwd(){ # To avoid error: channel 3: open failed: administratively prohibited: open failed
  sed -i 's/AllowTcpForwarding no/AllowTcpForwarding yes/' /etc/ssh/sshd_config
  service ssh restart
}
allowTcpFwd


# if the run was successful and the notebook was properly started:
echo -----------------------------------------------------------------------------------
echo "jupyter notebook launched (logs on the VM at /root/jupyterLogs)"
echo "To be able to access the Jupyter notebook on your machine run:"
echo "ssh -i PATH_TO_SSHKEY -N -f -L 8888:localhost:8888 USER@IP"
#echo "ssh -i $sshKey -N -f -L 8888:localhost:8888 $user@$ip"
echo -----------------------------------------------------------------------------------
