# Remove existing stuff, in case of existing anaconda installations (AWS when no root)
#rm -rf ~/*

wget https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh --directory-prefix=$HOME
bash ~/Anaconda3-2019.10-Linux-x86_64.sh -b # -u

# Initialize conda
#env:
export PATH=/root/anaconda3/bin:$PATH # TODO: now running as root but it should be ubuntu or centos
#export LD_LIBRARY_PATH="/usr/lib64/openmpi/lib/:/usr/local/cuda/lib64:/usr/local/lib:/usr/lib:/usr/local/cuda/extras/CUPTI/lib64:/usr/local/mpi/lib:/lib/:" # TODO: this overrides the one defined at nvidia_baseline.sh !
#export LD_LIBRARY_PATH="/usr/lib64/openmpi/lib/:/usr/local/lib:/usr/lib:/usr/local/mpi/lib:/lib/:" # TODO: needed?

conda init

####################
# BY HAND:
exec bash
####################

# Install pytorch and fastai
conda install -y -c pytorch -c fastai fastai

# Clone progressive_growing_of_gans repo
git clone https://github.com/tkarras/progressive_growing_of_gans
mv progressive_growing_of_gans ~
tensor1to2_PGG(){
    goHere=$PWD
    cp -r /root/progressive_growing_of_gans/ /root/progressive_growing_of_gans_original # create backup of tensorflow1 version
    cd /root/progressive_growing_of_gans
    for f in $(grep -rnil "import tensorflow as tf")
      do sed -i 's/import tensorflow as tf/import tensorflow.compat.v1 as tf/' $f
    done
    cd $goHere
}
#tensor1to2_PGG

# Add PYTHONPATH to bashrc
cp ~/.bashrc ~/.bashrc.bu ; echo export PYTHONPATH=/root/progressive_growing_of_gans >> ~/.bashrc # TODO: now running as root but it should be ubuntu or centos

#env:
export PYTHONPATH=/root/progressive_growing_of_gans

#Install requirements with pip # TODO: this works
#sed -i 's/tensorflow-gpu>=1.6.0/tensorflow-gpu==1.15.2/' ~/progressive_growing_of_gans/requirements-pip.txt
pip install -r ~/progressive_growing_of_gans/requirements-pip.txt # TODO: this showed some error # TODO 2: shouldnt it be pip3?

# To fix https://github.com/tensorflow/tensorflow/issues/23715
conda install -y -c anaconda cudnn # installs cudnn 7.6.5 as part of cuda
