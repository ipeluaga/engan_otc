# Download resources
original(){
  wget https://s3.cern.ch/swift/v1/engan/import_example.py
  wget https://s3.cern.ch/swift/v1/engan/karras2018iclr-celebahq-1024x1024.pkl
  python import_example.py
}
wget https://s3.cern.ch/swift/v1/engan/custom_import_example.py
wget https://s3.cern.ch/swift/v1/engan/Gnet.pkl
wget https://s3.cern.ch/swift/v1/engan/Dnet.pkl
wget https://s3.cern.ch/swift/v1/engan/Gsnet.pkl
chmod +x custom_import_example.py
./custom_import_example.py

# tensor1to2_example
#sed -i 's/import tensorflow as tf/import tensorflow.compat.v1 as tf\ntf.disable_eager_execution()/' import_example.py

# Run the fake celebrities example
#python import_example.py
