condaSetup(){
  # >>> conda initialize >>> (this is from the VM's ~/.bashrc, w/o this conda does not work)
  # !! Contents within this block are managed by 'conda init' !!
  __conda_setup="$('/opt/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
  if [ $? -eq 0 ]; then
      eval "$__conda_setup"
  else
      if [ -f "/opt/anaconda3/etc/profile.d/conda.sh" ]; then
          . "/opt/anaconda3/etc/profile.d/conda.sh"
      else
          export PATH="/opt/anaconda3/bin:$PATH"
      fi
  fi
  unset __conda_setup
  # <<< conda initialize <<<

  #Activate TF15 conda env:
  conda activate tf15 # TODO: this should be added to bashrc, otherwise it has to be done manually right after ssh'ing
}

progressive_growing_of_gans(){
  # Get progressive_growing_of_gans
  git clone https://github.com/tkarras/progressive_growing_of_gans
  mv progressive_growing_of_gans ~

  # Add PYTHONPATH to bashrc
  cp ~/.bashrc ~/.bashrc.bu ; echo export PYTHONPATH=/home/ubuntu/progressive_growing_of_gans >> ~/.bashrc

  #env:
  export PYTHONPATH=/home/ubuntu/progressive_growing_of_gans

  #Install requirements with pip (skipping tensorflow. Option --user installs only for the current user)
  sed -i 's/tensorflow-gpu>=1.6.0//' ~/progressive_growing_of_gans/requirements-pip.txt
  pip install --user -r ~/progressive_growing_of_gans/requirements-pip.txt
}

# Run celeb. example
example(){
  wget https://s3.cern.ch/swift/v1/engan/import_example.py
  wget https://s3.cern.ch/swift/v1/engan/karras2018iclr-celebahq-1024x1024.pkl
  python import_example.py
}

installJupyterDependencies(){
  #conda install -y -c pytorch -c fastai fastai # this install fastai 2 ! we need 1 # TODO: unable to install fastai 1 with conda, ask t-systems to do it
  pip3 install sklearn
  pip3 install git+https://github.com/fastai/fastai1.git # installs fastai v1. Has to be run outside the virtual environment
}

runJupyter(){
  # Run jupyter notebook # to kill it https://github.com/jupyter/notebook/issues/2844
  nohup jupyter-notebook --allow-root --no-browser > /home/ubuntu/jupyterLogs 2>&1 &

  # Wait for the notebook to be ready...
  sleep 10

  # Get the notebook endpoint
  jupyter-notebook list
}

allowTcpFwd(){ # To avoid error: channel 3: open failed: administratively prohibited: open failed
  sudo sed -i 's/AllowTcpForwarding no/AllowTcpForwarding yes/' /etc/ssh/sshd_config
  sudo service ssh restart
}

jupyterInfo(){
  # if the run was successful and the notebook was properly started:
  echo -----------------------------------------------------------------------------------
  echo "jupyter notebook launched (logs on the VM at /home/ubuntu/jupyterLogs)"
  echo "To be able to access the Jupyter notebook on your machine run:"
  echo "ssh -i PATH_TO_SSHKEY -N -f -L 8888:localhost:8888 USER@IP"
  #echo "ssh -i $sshKey -N -f -L 8888:localhost:8888 $user@$ip"
  echo -----------------------------------------------------------------------------------
}

condaSetup
progressive_growing_of_gans
example

installJupyterDependencies
runJupyter
allowTcpFwd
jupyterInfo
