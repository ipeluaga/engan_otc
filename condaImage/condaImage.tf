variable "public_ip" { default = "80.158.34.80" }
variable "key_pair" { default = "exo" }
variable "flavor_name" { default = "p2.2xlarge.8" } # "p2v.8xlarge.8" # NVIDIA V100 (3.25) / "p1.2xlarge.8" # NVIDIA P100 (2.19)
variable "security_groups" { default = ["default", "Sys-FullAccess"] }
variable "storageCapacity" { default = 100 }
variable "availabilityZone" { default = "eu-de-02" }
variable "vmName" { default = "i210" }
variable "sshKey" { default = "/home/ipelu/.ssh/id_rsa" }
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
variable "domainName" {  default = "OTC-EU-DE-00000000001000046175" }
variable "tenantName" {  default = "eu-de" }
variable "imageID" {
  #default = "6cc1f24a-f081-45c7-bd71-abe3fbc759f7" # Ubuntu 18.04 AI Environment
  default = "b1b0db52-4c6c-46d4-892d-ff7b23d46968" # OTC-Automated-AI-CondaImage
}
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
provider "opentelekomcloud" {
  access_key = "1I6OVQPECBE7KYCZHNG2"
  secret_key = "mTn9jLVTuMZmdYCEnwl4ixkZTLNCUW2Tj0irdr4p"
  domain_name = var.domainName
  tenant_name = var.tenantName
  auth_url    = "https://iam.eu-de.otc.t-systems.com/v3"
}
resource "opentelekomcloud_compute_instance_v2" "enganvm" {
  name = var.vmName
  flavor_name = var.flavor_name
  key_pair = var.key_pair
  security_groups = var.security_groups
  availability_zone = var.availabilityZone
  block_device {
    uuid                  = var.imageID
    source_type           = "image"
    volume_size           = var.storageCapacity
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
    volume_type           = "SATA"
  }
}
resource "opentelekomcloud_compute_floatingip_associate_v2" "myip" {
  floating_ip = var.public_ip
  instance_id = opentelekomcloud_compute_instance_v2.enganvm.id
}
resource null_resource "ai_stack_custom" {
  depends_on = [opentelekomcloud_compute_floatingip_associate_v2.myip]
  connection {
    host        = var.public_ip
    type        = "ssh"
    user        = "ubuntu"
    private_key = file(var.sshKey)
  }
  provisioner "file" {
   source      = "condaImage_config.sh"
   destination = "/tmp/condaImage_config.sh"
 }
 provisioner "remote-exec" {
   inline = [
     "bash /tmp/condaImage_config.sh",
   ]
 }
}
