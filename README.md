# OTC provisioning

## Requirements

-Linux OS

-jq CLI tool

-Python's pyyaml package

## 1) Clone this repository

```bash
git clone https://gitlab.cern.ch/ipeluaga/engan_otc.git
```

## 2) Create or import private key

Go to https://console.otc.t-systems.com/ecm/?region=eu-de&locale=en-us#/keypairs/manager/keypairsList . Note the permissions of the key must be set to 600:

```bash
sudo chmod 600 PATH_TO_SSHKEY
```

## 3) Get OTC's access and private keys

Go to https://console.otc.t-systems.com/iam/?region=eu-de&locale=en-us#/myCredential, click on the "Access Keys" tab and then "Create Access Key".

## 4) get a public IP on OTC

Go to https://console.otc.t-systems.com/vpc/?region=eu-de&locale=en-us#/eips/createEIP leave all as it is and click on "Assign Now" and then "Submit".

## 5) Configuration

Create a file "configs.yaml" for configurations, all properties are required. See the example file configs_example.yaml for reference:

**Variable** | **Explanation**
 --- | ---
vmName  |  Name for the machine
sshKey  |  Path to your ssh key, the one created or imported in step 2
key_pair  |  Name of your ssh key on OTC, from step 2
public_ip  |  IP from step 4
flavor_name  |  VM flavor name. For different GPU flavors see page 17 https://open-telekom-cloud.com/resource/blob/data/173316/a3ee46a8db939396117c3193d8253d9c/open-telekom-cloud-service-description.pdf . Note: this tool was tested on VMs with V100 and P100.
availabilityZone  |  Zone where the VM should be provisioned. One of eu-de-01, eu-de-02, eu-de-03.
security_groups  |  Security groups. Should be set to ["default", "Sys-FullAccess"]
storageCapacity  |  Storage capacity
access_key  |  Access key generated in step 3
secret_key  |  Secret key generated in step 3

## 6) Install Terraform

This tool uses Terraform to create the VM. Download the latest version and place the unzipped bin at /bin:

```bash
TERRAFORM_LATEST=$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r .current_version)
wget https://releases.hashicorp.com/terraform/$TERRAFORM_LATEST/terraform_${TERRAFORM_LATEST}_linux_amd64.zip
unzip terraform_${TERRAFORM_LATEST}_linux_amd64.zip
sudo mv terraform /bin
```

Check that the installation succeeded:

```bash
terraform version
```

## 7) Run

```bash
chmod +x main.py
./main.py
```
Use option '-c' to specify the location of your configurations file. If omitted, default is "configs.yaml".

Once the previous command finishes, ssh into the VM with the following command:

```bash
ssh -i PATH_TO_SSHKEY root@PUBLIC_IP
```
where:

PATH_TO_SSHKEY  |  configs.yaml's 'sshKey'
PUBLIC_IP  |  configs.yaml's 'public_ip'


## 8) Destroy VM when needed

```bash
./main.py --destroy
```
Use option '-c' to specify the location of your configurations file. If omitted, default is "configs.yaml".

## Troubleshooting

### Terraform's error: No valid host was found. There are not enough hosts available

That error is related to the availability zone. Try a different one at configs.yaml and run again. You can use the GUI (ECS instance create) to check whether the flavor you are trying to use is available on a given zone.
